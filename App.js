import React, {useEffect} from 'react';
import AppNavigation from './src/routes';
import firebase from '@react-native-firebase/app';
import OneSignal from 'react-native-onesignal';
import codePush from 'react-native-code-push';
import {Alert} from 'react-native';

var firebaseConfig = {
  apiKey: 'AIzaSyBlHSn4y0WQ3ETVTXD6Zupyf8atTSL2zHo',
  authDomain: 'sanbercode-2effb.firebaseapp.com',
  databaseURL: 'https://sanbercode-2effb.firebaseio.com',
  projectId: 'sanbercode-2effb',
  storageBucket: 'sanbercode-2effb.appspot.com',
  messagingSenderId: '333799621476',
  appId: '1:333799621476:web:a1c881526db15314fff071',
  measurementId: 'G-YJNE6V5EW1',
};

// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
  // firebase.analytics();
}

const App: () => React$Node = () => {
  useEffect(() => {
    console.log('ahai');
    OneSignal.setLogLevel(6, 0);

    OneSignal.init('408b7648-cb25-432e-96de-66f89974d5ee', {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.inFocusDisplaying(2);

    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);

    codePush.sync(
      {
        updateDialog: true,
        installMode: codePush.InstallMode.IMMEDIATE,
      },
      syncStatus,
    );

    return () => {
      OneSignal.removeEventListener('received', onReceived);
      OneSignal.removeEventListener('opened', onOpened);
      OneSignal.removeEventListener('ids', onIds);
    };
  }, []);

  const syncStatus = (status) => {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log('Checking for updates');
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log('Downloading package');
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        console.log('Up to date');
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log('Installing update');
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        Alert.alert('Notification', 'Update installed');
        break;
      case codePush.SyncStatus.AWAITING_USER_ACTION:
        console.log('Awaiting user action');
        break;
      default:
        break;
    }
  };

  const onReceived = (notification) => {
    console.log('Notification received: ', notification);
  };

  const onOpened = (openResult) => {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  };

  const onIds = (device) => {
    console.log('Device info: ', device);
  };

  return <AppNavigation />;
};

export default App;
