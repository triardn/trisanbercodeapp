import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import SplashScreen from './screens/Splashscreen/Splashcreen';
import Intro from './screens/Intro/Intro';
import Login from './screens/Login/Login';
import Profile from './screens/Profile/Profile';
import Home from './screens/Home/Home';
import Maps from './screens/Maps/Maps';
import Register from './screens/Register/Register';
import Class from './screens/Class/Class';
import Chat from './screens/Chat/Chat';

import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicon from 'react-native-vector-icons/Ionicons';

import Asyncstorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const TabNavigation = () => (
  <Tab.Navigator
    screenOptions={({route}) => ({
      tabBarIcon: ({focused, color, size}) => {
        let iconName;
        if (route.name === 'Profile') {
          iconName = 'account-circle-outline';
        } else if (route.name === 'Home') {
          iconName = 'home-outline';
        } else if (route.name === 'Maps') {
          iconName = 'map-marker-circle';
        } else if (route.name === 'Chat') {
          iconName = 'chat';
        }

        return (
          <MaterialCommunityIcon name={iconName} size={size} color={color} />
        );
      },
    })}
    tabBarOptions={{
      activeTintColor: '#3EC6FF',
      inactiveTintColor: 'gray',
    }}>
    <Tab.Screen name="Home" component={Home} />
    <Tab.Screen name="Maps" component={Maps} />
    <Tab.Screen name="Chat" component={Chat} />
    <Tab.Screen name="Profile" component={Profile} />
  </Tab.Navigator>
);

const MainNavigation = ({isLogin}) => (
  <Stack.Navigator initialRouteName={isLogin ? 'Home' : 'Intro'}>
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Login"
      component={Login}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Home"
      component={TabNavigation}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Register"
      component={Register}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Class"
      component={Class}
      options={{headerShown: true}}
    />
  </Stack.Navigator>
);

function AppNavigation() {
  const [isLoading, setIsLoading] = React.useState(true);
  const [isLogin, setIsLogin] = React.useState(false);

  //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
  React.useEffect(() => {
    const checkIsLogin = async () => {
      try {
        const login = await Asyncstorage.getItem('isLogin');
        if (login === 'true') {
          setIsLogin(true);
        }
      } catch (err) {
        console.log('checkIsLogin err: ', err);
      }
    };
    checkIsLogin();

    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  return (
    <NavigationContainer>
      <MainNavigation isLogin={isLogin} />
    </NavigationContainer>
  );
}

export default AppNavigation;
