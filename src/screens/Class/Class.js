import React, {useState} from 'react';
import {View, processColor} from 'react-native';
import {BarChart} from 'react-native-charts-wrapper';

const populateData = (data) => {
  let result = [];
  for (let i = 0; i < data.length; i++) {
    const marker = getMarker(data[i]);
    const newData = {
      y: data[i],
      marker: marker,
    };

    result.push(newData);
  }

  return result;
};

const getMarker = (arr) => {
  let marker = [];
  arr.map((data, index) => {
    if (index === 0) {
      marker.push(`React Native Dasar ${data}`);
    } else {
      marker.push(`React Native Lanjutan ${data}`);
    }
  });

  return marker;
};

const Class = ({navigation}) => {
  const data = [
    [100, 40],
    [80, 60],
    [40, 90],
    [78, 45],
    [67, 87],
    [98, 32],
    [150, 90],
  ];

  const [legend, setLegend] = useState({
    enabled: true,
    textSize: 14,
    form: 'SQUARE',
    formSize: 14,
    xEntrySpace: 10,
    yEntrySpace: 5,
    formToTextSpace: 5,
    wordWrapEnabled: true,
    maxSizePercent: 0.5,
  });

  const [chart, setChart] = useState({
    data: {
      dataSets: [
        {
          values: populateData(data),
          label: '',
          config: {
            colors: [processColor('#3EC6FF'), processColor('#088dc4')],
            stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
            drawFilled: false,
            drawValues: false,
          },
        },
      ],
    },
  });

  const [xAxis, setXAxis] = useState({
    valueFormatter: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Agu',
      'Sep',
      'Oct',
      'Nov',
      'Des',
    ],
    position: 'BOTTOM',
    drawAxisLine: true,
    drawGridLines: false,
    axisMinimum: -0.5,
    granularityEnabled: true,
    granularity: 1,
    axisMaximum: new Date().getMonth(),
    spaceBetweenLabels: 0,
    labelRotationAngle: -45.0,
    limitLines: [{limit: 115, lineColor: processColor('red'), lineWidth: 1}],
  });

  const [yAxis, setYAxis] = useState({
    left: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      drawGridLines: false,
    },
    right: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      enabled: false,
    },
  });

  return (
    <View style={{flex: 1}}>
      <BarChart
        style={{flex: 1}}
        data={chart.data}
        yAxis={yAxis}
        xAxis={xAxis}
        doubleTapToZoomEnabled={false}
        pinchZoom={false}
        chartDescription={{text: ''}}
        legend={legend}
        marker={{
          enabled: true,
          markerColor: 'grey',
          textColor: 'white',
          textSize: 14,
        }}
      />
    </View>
  );
};

export default Class;
