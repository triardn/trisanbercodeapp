import React, {useContext} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RootContext} from '../../index';
import {styles} from '../../style/styles';

const DismissKeyboard = ({children}) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

const Todo = () => {
  const state = useContext(RootContext);

  const renderItem = ({item, index}) => {
    return (
      <View style={styles.container}>
        <View style={styles.itemContainer}>
          <View style={styles.itemTodo}>
            <View style={styles.itemText}>
              <Text>{item.date}</Text>
              <Text>{item.note}</Text>
            </View>
            <View style={styles.itemDeleteIcon}>
              <TouchableOpacity onPress={() => state.deleteNote(item.id)}>
                <Icon name="trash-can-outline" size={40} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  };

  return (
    <DismissKeyboard>
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <Text>Masukan Todolist</Text>
          <View style={styles.textInputContainer}>
            <TextInput
              style={styles.textInput}
              placeholder="Input here"
              underlineColorAndroid="transparent"
              onChangeText={(note) => state.handleChangeInput(note)}
              value={state.input}
            />
            <TouchableOpacity
              style={styles.buttonAdd}
              onPress={() => state.addNotes()}>
              <Icon name="plus" size={30} />
            </TouchableOpacity>
          </View>
          {/* <ScrollView>{allnotes}</ScrollView> */}
          <FlatList
            data={state.notes}
            renderItem={renderItem}
            keyExtractor={(item) => item.id.toString()}
          />
        </View>
      </View>
    </DismissKeyboard>
  );
};

export default Todo;
