import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {styles} from '../../style/styles';

const DismissKeyboard = ({children}) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

const Todo = () => {
  const [notes, modifyNotes] = useState([]);

  const [note, modifyNote] = useState('');

  let allnotes = notes.map((val, key) => {
    return (
      <Item
        key={key}
        keyval={key}
        val={val}
        deleteMethod={() => deleteNote(val.id)}
      />
    );
  });

  const addNote = () => {
    if (note) {
      const d = new Date();
      const currentLength = notes.length;
      notes.push({
        id: currentLength + 1,
        date: d.getFullYear() + '/' + (d.getMonth() + 1) + '/' + d.getDate(),
        note: note,
      });
    }

    modifyNotes(notes);
    modifyNote('');
  };

  const deleteNote = (key) => {
    console.log(notes[key]);
    console.log(key);
    // notes.splice(key, 1);
    modifyNotes(notes.filter((note) => note.id !== key));
  };

  return (
    <DismissKeyboard>
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <Text>Masukan Todolist</Text>
          <View style={styles.textInputContainer}>
            <TextInput
              style={styles.textInput}
              placeholder="Input here"
              underlineColorAndroid="transparent"
              onChangeText={(note) => modifyNote(note)}
              value={note}
            />
            <TouchableOpacity
              style={styles.buttonAdd}
              onPress={addNote.bind(this)}>
              <Icon name="plus" size={30} />
            </TouchableOpacity>
          </View>
          <ScrollView>{allnotes}</ScrollView>
        </View>
      </View>
    </DismissKeyboard>
  );
};

const Item = (props) => {
  return (
    <View style={styles.container} key={props.keyval}>
      <View style={styles.itemContainer}>
        <View style={styles.itemTodo}>
          <View style={styles.itemText}>
            <Text>{props.val.date}</Text>
            <Text>{props.val.note}</Text>
          </View>
          <View style={styles.itemDeleteIcon}>
            <TouchableOpacity onPress={props.deleteMethod}>
              <Icon name="trash-can-outline" size={40} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Todo;
