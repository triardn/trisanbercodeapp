import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {styles} from '../../style/styles';

export default class Intro extends Component {
  render() {
    return (
      <View style={styles.introContainer}>
        <Text>Hello Kelas React Native Lanjutan Sanbercode!</Text>
      </View>
    );
  }
}
