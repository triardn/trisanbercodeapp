import React, {useEffect, useState} from 'react';
import {View, Image, StyleSheet} from 'react-native';

import Asyncstorage from '@react-native-community/async-storage';

function SplashScreen({navigation}) {
  // const [isSkipIntro, setIsSkipIntro] = useState('');
  //
  // useEffect(() => {
  //   async function skipIntro() {
  //     try {
  //       const skip = await Asyncstorage.getItem('skip-intro');
  //       setIsSkipIntro(skip);
  //     } catch (err) {
  //       console.log('failed to get skip-intro: ', err);
  //     }
  //   }
  //   skipIntro();

  //   return () => {
  //     if (isSkipIntro) {
  //       navigation.reset({
  //         index: 0,
  //         routes: [{name: 'Login'}],
  //       });
  //     }
  //   };
  // }, []);

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image source={require('../../assets/images/logo.jpg')} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  logoContainer: {
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SplashScreen;
