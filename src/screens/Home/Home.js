import React from 'react';
import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';
import {styles} from './styles';
import Ionicon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const Home = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.boxClassContainer}>
          <View style={styles.itemClassContainer}>
            <View style={styles.itemClass}>
              <TouchableOpacity onPress={() => navigation.navigate('Class')}>
                <Ionicon name="md-logo-react" size={70} color="#FFFFFF" />
                <Text>React Native</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.itemClass}>
              <Ionicon name="md-logo-python" size={70} color="#FFFFFF" />
              <Text>Data Science</Text>
            </View>
            <View style={styles.itemClass}>
              <Ionicon name="md-logo-react" size={70} color="#FFFFFF" />
              <Text>React JS</Text>
            </View>
            <View style={styles.itemClass}>
              <Ionicon name="md-logo-laravel" size={70} color="#FFFFFF" />
              <Text>Laravel</Text>
            </View>
          </View>
        </View>
        <View style={styles.firstHeaderClassContainer}>
          <Text style={styles.headerText}>Kelas</Text>
        </View>

        <View style={styles.boxClassContainer}>
          <View style={styles.itemClassContainer}>
            <View style={styles.itemClass}>
              <Ionicon name="md-logo-wordpress" size={70} color="#FFFFFF" />
              <Text>Wordpress</Text>
            </View>
            <View style={styles.itemClass}>
              <Image
                source={require('../../assets/images/website-design.png')}
                style={styles.imageIcon}
              />
              <Text>Desain Grafis</Text>
            </View>
            <View style={styles.itemClass}>
              <MaterialCommunityIcon
                name="server"
                size={70}
                color="#FFFFFF"
                style={styles.marginBottomFive}
              />
              <Text>Web Server</Text>
            </View>
            <View style={styles.itemClass}>
              <Image
                source={require('../../assets/images/ux.png')}
                style={styles.imageIcon}
              />
              <Text>UI/UX Design</Text>
            </View>
          </View>
        </View>
        <View style={styles.secondHeaderClassContainer}>
          <Text style={styles.headerText}>Kelas</Text>
        </View>

        <View style={styles.summaryListContainer}>
          <View style={styles.summaryHeaderContainer}>
            <Text style={styles.summaryTitle}>Summary</Text>
          </View>
          <View style={styles.container}>
            {/* React Native */}
            <View>
              <View style={styles.summaryItemBackgroundBlue}>
                <Text style={styles.classTitle}>React Native</Text>
              </View>
              <View style={styles.summaryItemBackgroundDarkBlue}>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Today</Text>
                  <Text style={styles.textWhite}>20 Orang</Text>
                </View>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Total</Text>
                  <Text style={styles.textWhite}>100 Orang</Text>
                </View>
              </View>
            </View>

            {/* Data Science */}
            <View>
              <View style={styles.summaryItemBackgroundBlue}>
                <Text style={styles.classTitle}>React Native</Text>
              </View>
              <View style={styles.summaryItemBackgroundDarkBlue}>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Today</Text>
                  <Text style={styles.textWhite}>30 Orang</Text>
                </View>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Total</Text>
                  <Text style={styles.textWhite}>100 Orang</Text>
                </View>
              </View>
            </View>

            {/* React JS */}
            <View>
              <View style={styles.summaryItemBackgroundBlue}>
                <Text style={styles.classTitle}>React Native</Text>
              </View>
              <View style={styles.summaryItemBackgroundDarkBlue}>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Today</Text>
                  <Text style={styles.textWhite}>66 Orang</Text>
                </View>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Total</Text>
                  <Text style={styles.textWhite}>100 Orang</Text>
                </View>
              </View>
            </View>

            {/* Laravel */}
            <View>
              <View style={styles.summaryItemBackgroundBlue}>
                <Text style={styles.classTitle}>React Native</Text>
              </View>
              <View style={styles.summaryItemBackgroundDarkBlue}>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Today</Text>
                  <Text style={styles.textWhite}>60 Orang</Text>
                </View>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Total</Text>
                  <Text style={styles.textWhite}>100 Orang</Text>
                </View>
              </View>
            </View>

            {/* Wordpress */}
            <View>
              <View style={styles.summaryItemBackgroundBlue}>
                <Text style={styles.classTitle}>React Native</Text>
              </View>
              <View style={styles.summaryItemBackgroundDarkBlue}>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Today</Text>
                  <Text style={styles.textWhite}>73 Orang</Text>
                </View>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Total</Text>
                  <Text style={styles.textWhite}>243 Orang</Text>
                </View>
              </View>
            </View>

            {/* Desain Grafis */}
            <View>
              <View style={styles.summaryItemBackgroundBlue}>
                <Text style={styles.classTitle}>React Native</Text>
              </View>
              <View style={styles.summaryItemBackgroundDarkBlue}>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Today</Text>
                  <Text style={styles.textWhite}>113 Orang</Text>
                </View>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Total</Text>
                  <Text style={styles.textWhite}>309 Orang</Text>
                </View>
              </View>
            </View>

            {/* Web Server */}
            <View>
              <View style={styles.summaryItemBackgroundBlue}>
                <Text style={styles.classTitle}>React Native</Text>
              </View>
              <View style={styles.summaryItemBackgroundDarkBlue}>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Today</Text>
                  <Text style={styles.textWhite}>7 Orang</Text>
                </View>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Total</Text>
                  <Text style={styles.textWhite}>51 Orang</Text>
                </View>
              </View>
            </View>

            {/* UI/UX Design */}
            <View>
              <View style={styles.summaryItemBackgroundBlue}>
                <Text style={styles.classTitle}>React Native</Text>
              </View>
              <View style={styles.summaryItemBackgroundDarkBlue}>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Today</Text>
                  <Text style={styles.textWhite}>111 Orang</Text>
                </View>
                <View style={styles.classRowItem}>
                  <Text style={styles.classRowItemKey}>Total</Text>
                  <Text style={styles.textWhite}>408 Orang</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.footerSummaryListContainer} />
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;
