import React, {useState, createContext} from 'react';
import Todo from './screens/Todo/TodoList-Context';
import Intro from './screens/Intro/Intro-1';
import Profile from './screens/Profile/Profile';
import Todos from './screens/Todo/TodoList';

export const RootContext = createContext();

const Context = () => {
  const [input, setInput] = useState('');
  const [notes, setNotes] = useState([]);

  const handleChangeInput = (value) => {
    setInput(value);
  };

  const addNotes = () => {
    const day = new Date().getDay();
    const month = new Date().getMonth();
    const year = new Date().getFullYear();
    const today = `${day}/${month}/${year}`;

    // get last id
    const latestData = notes.length;

    // add notes
    setNotes([...notes, {id: latestData + 1, note: input, date: today}]);
    setInput('');
  };

  const deleteNote = (id) => {
    setNotes(notes.filter((note) => note.id !== id));
  };

  return (
    <RootContext.Provider
      value={{input, notes, handleChangeInput, addNotes, deleteNote}}>
      <Todo />
    </RootContext.Provider>
  );
};

export default Context;
