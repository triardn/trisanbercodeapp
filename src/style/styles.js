import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    marginVertical: 40,
    marginHorizontal: 20,
  },
  textInputContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  textInput: {
    borderWidth: 1,
    width: '85%',
  },
  buttonAdd: {
    backgroundColor: '#3EC6FF',
    width: '13%',
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemContainer: {
    borderRadius: 2,
    borderWidth: 5,
    height: 80,
    marginVertical: 20,
    borderColor: '#DDDDDD',
  },
  itemTodo: {
    flexDirection: 'row',
    marginTop: 15,
    marginLeft: 10,
  },
  itemText: {
    flex: 1,
  },
  itemDeleteIcon: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 10,
  },
  onBoardingContainer: {
    flex: 1,
  },
  sldes: {
    flex: 1,
  },
  title: {
    color: '#191970',
    fontSize: 24,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  text: {
    color: '#a4a4a6',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16,
    marginHorizontal: 20,
  },
  image: {
    marginVertical: 40,
    alignSelf: 'center',
  },
  buttonCircle: {
    borderRadius: 100,
    width: 45,
    height: 45,
    backgroundColor: '#191970',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
